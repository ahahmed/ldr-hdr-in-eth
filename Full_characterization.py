import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import sys, select
import shutil
from shutil import copytree, ignore_patterns

import fnmatch
import os
import re



#
#

'''Optimal'''
#Trim_chip =  '0b0110010101'  # ETH1
Trim_chip = '1100011000'   	  # ETH2

	
'''Trimbit for LINEAR FE'''
#Trim_chip_lin =   '0b0110010101'  #ETH1
Trim_chip_lin = '1100011110'       #ETH 2




def removecontent(path):
	if os.path.exists(path):
		try:
			os.system("rm -r "+ path )
		except Exception as e:
			pass

def PowertrimLIN():
	os.system('python trimfunc.py --register="VOLTAGE_TRIM" --binary='+Trim_chip_lin)

def PowertrimObtimal():
	os.system('python trimfunc.py --register="VOLTAGE_TRIM" --binary=' +Trim_chip)


#It goes through all the defaul_chip files generated
def ChipCharact():
	directory = '00_TEST_INFO'+ '/' +'Chip_Characterization'
	currentdir = os.getcwd()
	if not os.path.exists(directory):
    		os.makedirs(directory)


	t0 = time.time()
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	testTag = ('Full_Characterization')
	#testTag = 'irradiation_'

	#testTag = 'AfterDoseOf_100Mrad_RoomT_'
	directory = directory + '/' + testTag + '_' + timestamp
	if not os.path.exists(directory):
		os.makedirs(directory)





		# power cycle
		pass

	
	#Lin FE:
	try:
		PowertrimLIN() # Trimming DAC optimal VDDA and VDDD	
		os.rename('default_chip_lin.yaml','default_chip.yaml')
		'''
		Make two copies of Tdac DIFF mask files to ldr-software folder and call them: lin_Tdac_metatune_mask.h5  and 			lin_Tdac_metatune_mask_tune_ini.h5
		'''
		'''
		using linear triming TDACs before from previous run for a threshodls scan:
		'''
		if not os.path.exists('output_data'):
			os.makedirs("output_data")
		'''Threshold tuning with previous mask '''
		shutil.copy("lin_Tdac_metatune_mask" + ".h5", "output_data")
		os.system("bdaq53 scan_noise_occupancy_lin")
		os.system("bdaq53 scan_threshold_lin") 
		os.system("bdaq53 meta_tune_local_threshold_lin")
		os.system("bdaq53 scan_analog_lin")
		for file in os.listdir('output_data'):
			if file.endswith("mask.h5"):
				shutil.copy("output_data/" +file, "lin_Tdac_metatune_mask" + ".h5")
		shutil.move("output_data", directory + '/LIN/lin_meta_tune_threshold')

		if not os.path.exists('output_data'):
			os.makedirs("output_data")

		shutil.copy("lin_Tdac_metatune_mask_tune_ini" + ".h5", "output_data")
		os.system("bdaq53 scan_noise_occupancy_lin")
		os.system("bdaq53 scan_threshold_lin")
		shutil.move("output_data", directory + '/LIN/lin_threshold_scan_inital_tuning')
		os.rename('default_chip.yaml','default_chip_lin.yaml')
	except Exception as e:
		raise
	finally:
		pass


	#Diff FE:
	try:
		PowertrimObtimal()
		os.rename('default_chip_diff.yaml','default_chip.yaml')
		'''
		Make two copies of Tdac DIFF mask files to ldr-software folder and call them: 'diff_Tdac_metatune_mask.h5'  and 		'diff_Tdac_metatune_mask_tune_ini.h5'
		'''
		if not os.path.exists('output_data'):
			os.makedirs("output_data")

		'''Threshold tuning with #with previous mask '''		
		shutil.copy("diff_Tdac_metatune_mask" + ".h5", "output_data")
		os.system("bdaq53 scan_noise_occupancy_diff")
		os.system("bdaq53 scan_threshold_diff")
		os.system("bdaq53 meta_tune_local_threshold_diff")
		os.system("bdaq53 scan_analog_diff")
		for file in os.listdir('output_data'):
			if file.endswith("mask.h5"):
				shutil.copy("output_data/" +file, "diff_Tdac_metatune_mask" + ".h5")
		shutil.move("output_data", directory + '/DIFF/diff_meta_tune_threshold')
					
		''' using linear triming TDACs before irradiation for a threshold scan: '''
		if not os.path.exists('output_data'):
			os.makedirs("output_data")

		shutil.copy("diff_Tdac_metatune_mask_tune_ini" + ".h5", "output_data")
		os.system("bdaq53 scan_noise_occupancy_diff")
		os.system("bdaq53 scan_threshold_diff")
		shutil.move("output_data", directory + '/DIFF/diff_threshold_scan_inital_tuning')
		os.rename('default_chip.yaml','default_chip_diff.yaml')
	except Exception as e:
		raise
	finally:
		pass

	#Diff FE for 100 Mrad:
	try:
		PowertrimObtimal()
		os.rename('default_chip_differential100Mrad.yaml' , 'default_chip.yaml')
		'''
		Make two copies of Tdac DIFF mask files to ldr-software folder and call them: 'diff_Tdac_metatune_mask.h5'  and 		'diff_Tdac_metatune_mask_tune_ini.h5'
		'''

		if not os.path.exists('output_data'):
			os.makedirs("output_data")

		'''Threshold tuning with #with previous mask '''		
		shutil.copy("diff_Tdac_metatune_mask_100mrad" + ".h5", "output_data")
		os.system("bdaq53 scan_noise_occupancy_diff")
		os.system("bdaq53 scan_threshold_diff")
		os.system("bdaq53 meta_tune_local_threshold_diff")
		os.system("bdaq53 scan_analog_diff")
		for file in os.listdir('output_data'):
			if file.endswith("mask.h5"):
				shutil.copy("output_data/" +file, "diff_Tdac_metatune_mask_100mrad" + ".h5")
		shutil.move("output_data", directory + '/DIFF_100mrad/diff_meta_tune_threshold')
					
		''' using linear triming TDACs before irradiation for a threshold scan: '''
		if not os.path.exists('output_data'):
			os.makedirs("output_data")

		shutil.copy("diff_Tdac_metatune_mask_100mrad_tune_ini" + ".h5", "output_data")
		os.system("bdaq53 scan_noise_occupancy_diff")
		os.system("bdaq53 scan_threshold_diff")
		shutil.move("output_data", directory + '/DIFF_100mrad/diff_threshold_scan_inital_tuning')
		os.rename('default_chip.yaml','default_chip_differential100Mrad.yaml')
	except Exception as e:
		raise
	finally:
		pass


	# tend = time.time()
	# print 'Total time needed: ', tend-t0

if __name__ == '__main__':
	 ChipCharact()
