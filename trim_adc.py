from bdaq53.rd53a import RD53A
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.fifo_readout import FifoReadout
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import argparse
import logging
import os
import time




import time
import numpy as np

from tqdm import tqdm
from threading import Timer

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis import plotting
  


'''
Used to write trimming-bit for mainly register: VOLTAGE_TRIM

'''

class Trim_adc(ScanBase):
    scan_id = "ext_trigger_scan"

    def __init__(self,**kwargs):
        self.chip=RD53A()
        #self.chip.power_on(**kwargs)
        self.chip.init()
        self.fifo_readout = FifoReadout(self.chip)
        self.chip.init_communication()
        self.chip.set_dacs(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()




    def write_register(self, Data):
        self.chip.write_register("MONITOR_CONFIG",0b1 + Data + 000101, write=True)
        #self.chip.write_register("MONITOR_CONFIG","0b111001000101", write=True)


if __name__ == "__main__":
    # bin_list = [11110]
    bin_list = [11010, 11011, 11100, 11101, 11110, 11111]
    #bin_list = [str(item) for item in bin_list]
    #bin_list = int(bin_list, 2)
    

    for item in bin_list:
        TrimLDR_instance = Trim_adc()
        TrimLDR_instance.write_register(item)
        TrimLDR_instance.chip.close()
        time.sleep(2)
        os.system("python temp_rad_monitor.py ")
