from bdaq53.rd53a import RD53A
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.fifo_readout import FifoReadout
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import argparse


'''
Used to write trimming-bit for mainly register: VOLTAGE_TRIM

'''

class Trimclass():
    def __init__(self,**kwargs):
        self.chip=RD53A()
        #self.chip.power_on(**kwargs)
        self.chip.init()
        self.fifo_readout = FifoReadout(self.chip)
        self.chip.init_communication()
        self.chip.set_dacs(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
	#self.chip.write_register("VOLTAGE_TRIM",0b + 0110010101, write=True)
  

    def write_register(self, reg, Data):
        self.chip.write_register(reg,Data, write=True)
        Data = 11111 # chip LDR1
        # Data = 11111 # chip LDR2000101
        self.chip.write_register("MONITOR_CONFIG",0b1+Data+000101, write=True)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="example: python trimfunc.py --register=VOLTAGE_TRIM --binary=0b1000011001")
    parser.add_argument(
    '-r','--register',
    help=" Register in which it should be written to",
    type=str,
)
    parser.add_argument(
    '-b','--binary',
    help="binary timming bits",
    type=str,
)
    arguments = parser.parse_args()

    register = arguments.register 
    data     = arguments.binary 


    data = int(data, 2)
    arguments = parser.parse_args()
    TrimLDR_instance = Trimclass()
    TrimLDR_instance.write_register(register,data)


