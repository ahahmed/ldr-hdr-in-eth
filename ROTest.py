

import numpy
import yaml
import time
import os
import tables as tb
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
from bdaq53.fifo_readout import FifoReadout
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
from bdaq53.scan_base_RO import ScanBase_Ring_O
from bdaq53.register_utils import RD53ARegisterParser
from bdaq53.analysis import analysis_utils
from bitarray import bitarray
from datetime import datetime
import matplotlib
import matplotlib.pyplot as plt
#from mon1 import monitor_datalogger_power_supply
import basil
import visa
from operator import itemgetter
import ast
import sys, termios, tty, os, time
import csv
import collections
from tqdm import tqdm
import csv
from bdaq53.register_utils import RD53ARegisterParser
from bitarray import bitarray
from collections import OrderedDict
'''
# ETH 2
trimvoltage_dict = collections.OrderedDict()
trimvoltage_dict["Vtrim_digital_optimal"]    = '0b1011111000'
trimvoltage_dict["Vtrim_digital_+Onebit"]    = '0b1011111001'
trimvoltage_dict["Vtrim_digital_+twobits"]   = '0b1011111010'
trimvoltage_dict["Vtrim_digital_+Threebits"] = '0b1011111011'
trimvoltage_dict["Vtrim_digital_-Onebit"]    = '0b1011110101'
trimvoltage_dict["Vtrim_digital_-twobit"]    = '0b1011110100'
'''

# ETH 1
trimvoltage_dict = collections.OrderedDict()
trimvoltage_dict["Vtrim_digital_optimal"]    = '0b1100011000'
trimvoltage_dict["Vtrim_digital_+Onebit"]    = '0b1100011001'
trimvoltage_dict["Vtrim_digital_+twobits"]   = '0b1100011010'
trimvoltage_dict["Vtrim_digital_+Threebits"] = '0b1100011011'
trimvoltage_dict["Vtrim_digital_-Onebit"]    = '0b1100010101'
trimvoltage_dict["Vtrim_digital_-twobit"]    = '0b1100010100'


chip_configuration = 'default_chip.yaml'

def removecontent(path):
	if os.path.exists(path):
		try:
			os.system("rm -r "+ path )
		except Exception as e:
			pass


chip_configuration = 'default_chip.yaml'

local_configuration = {
    # Hardware settings
    'VDDA'              : 1.25,
    'VDDD'              : 1.25,
    'VDDA_cur_lim'      : 0.7,
    'VDDD_cur_lim'      : 0.7,
}   

class RingOscTest(ScanBase_Ring_O):
    scan_id = "RingOscTest"

    def configure(self, **kwargs):
        super(RingOscTest, self).configure(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
    
    def write_global_pulse(self, width, chip_id=0, write=True):
        indata = [self.chip.CMD_GLOBAL_PULSE] * 2  
        chip_id_bits = chip_id << 1
        indata += [self.chip.cmd_data_map[chip_id_bits]]
        width_bits = width << 1
        indata += [self.chip.cmd_data_map[width_bits]]
        if write:
            self.chip.write_command(indata)
        return indata

    def write_ring_oscillator(self, value = 0):
        self.chip.write_register(register=110, data=value, write=True)
        self.chip.write_register(register=111, data=value, write=True)
        self.chip.write_register(register=112, data=value, write=True)
        self.chip.write_register(register=113, data=value, write=True)
        self.chip.write_register(register=114, data=value, write=True)
        self.chip.write_register(register=115, data=value, write=True)
        self.chip.write_register(register=116, data=value, write=True)
        self.chip.write_register(register=117, data=value, write=True)

    def read_ring_oscillator(self):
        self.chip.read_register(register=110, write=True)
        self.chip.read_register(register=111, write=True)
        self.chip.read_register(register=112, write=True)
        self.chip.read_register(register=113, write=True)
        self.chip.read_register(register=114, write=True)
        self.chip.read_register(register=115, write=True)
        self.chip.read_register(register=116, write=True)
        self.chip.read_register(register=117, write=True)


    def scan(self,  pulse_width = 7,  **kwargs):
        directory = '00_TEST_INFO'
        if not os.path.exists(directory):
            os.makedirs(directory)

        directory = directory + '/ring_oscillators/ETH'
        timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
        if not os.path.exists(directory):
            os.makedirs(directory)

        self.results = {}
        self.count1 = {}
        self.count2 = {}
        pulse_duration = [6.4, 12.8, 25.6, 51.2, 102.4, 204.8, 409.6, 819.2, 1638.4, 3276.8]

        self.logger.info('Starting scan...')
        self.chip.write_register(register=109, data=0xFF, write=True)
        self.write_ring_oscillator(value = 0)
        with self.readout(fill_buffer=True, clear_buffer=False):

            indata = self.chip.write_register(register='GLOBAL_PULSE_ROUTE', data=0x2000, write=False)
            indata += self.write_global_pulse(width=pulse_width, write=True)
            self.chip.write_command(indata)
            self.read_ring_oscillator()
            self.chip.write_register(register=109, data=0x00, write=True)
            self.write_ring_oscillator(value = 0)
            self.logger.info('Scan finished')

    def Ring_oscillator_data(self, ring_oscillators, Temperature, filer_name, dose):
        ''' Log all the error to a txt file '''
        RO = '00_TEST_INFO/RO_data/'
        if not os.path.exists(RO):
            os.makedirs(RO)
        RO_data = RO +'/' +  'Ring_oscillator_data_'+ filer_name +'.csv'
        timestamp = time.time()
	print len(ring_oscillators)
	if len(ring_oscillators)==8:
		Mdata =[[timestamp, Temperature ,dose, ring_oscillators[0], ring_oscillators[1], ring_oscillators[2], 
		                                  ring_oscillators[3], ring_oscillators[4], ring_oscillators[5], 
		                                  ring_oscillators[6], ring_oscillators[7]
		 ]]
		file = open(RO_data, 'a')
		with file:
		    writer = csv.writer(file)
		    writer.writerows(Mdata)
		    file.close()
	else:
		pass


    def analyze(self):
        Temperature = self.chip._measure_temperature_ntc_CERNFMC()
        with tb.open_file(self.output_filename + '.h5', 'r+') as out_file_h5:
            raw_data = out_file_h5.root.raw_data[:]
            userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(raw_data))
            RO_data_list = []
            RO_name_list = []
            
            #print userk_data

            if len(userk_data) == 0:
                raise IOError('Received no data from the chip!')
            #if len(userk_data) == 8:
            for elements in userk_data:
                #print elements[2]
                RO_name_list.append
                RO_data_list.append(elements[2] & ((1<<12)-1))
        RO_data_list = self.del_duplicate(RO_data_list)
        return RO_data_list, Temperature


    def del_duplicate(self, x):
        return list(OrderedDict.fromkeys(x))



    def set_trim(self, trimbits):
        os.system('python trimfunc.py --register="VOLTAGE_TRIM" --binary='+trimbits)

    def main(self, dose):
        with open(chip_configuration, 'r') as f:
            configuration = yaml.load(f)
            configuration.update(local_configuration)


        self.set_trim(trimvoltage_dict["Vtrim_digital_optimal"])   
        scan = RingOscTest(record_chip_status=False)
        scan.start(**configuration)
        RO_data, Temperature  = scan.analyze()
        scan.close()
        filer_name = "Vtrim_digital_optimal" 
        self.Ring_oscillator_data(RO_data, Temperature, filer_name, dose)
        removecontent("output_data")


        self.set_trim(trimvoltage_dict["Vtrim_digital_+Onebit"])   
        scan = RingOscTest(record_chip_status=False)
        scan.start(**configuration)
        RO_data, Temperature  = scan.analyze()
        scan.close()
        filer_name = "Vtrim_digital_+Onebit"
        self.Ring_oscillator_data(RO_data, Temperature, filer_name, dose)
        removecontent("output_data")


        self.set_trim(trimvoltage_dict["Vtrim_digital_+twobits"])   
        scan = RingOscTest(record_chip_status=False)
        scan.start(**configuration)
        RO_data, Temperature  = scan.analyze()
        scan.close()
        filer_name = "Vtrim_digital_+twobits"
        self.Ring_oscillator_data(RO_data, Temperature, filer_name, dose)
        removecontent("output_data")


        self.set_trim(trimvoltage_dict["Vtrim_digital_+Threebits"])   
        scan = RingOscTest(record_chip_status=False)
        scan.start(**configuration)
        RO_data, Temperature  = scan.analyze()
        scan.close()
        filer_name = "Vtrim_digital_+Threebits"
        self.Ring_oscillator_data(RO_data, Temperature, filer_name, dose)
        removecontent("output_data")


        self.set_trim(trimvoltage_dict["Vtrim_digital_-Onebit"])   
        scan = RingOscTest(record_chip_status=False)
        scan.start(**configuration)
        RO_data, Temperature  = scan.analyze()
        scan.close()
        filer_name = "Vtrim_digital_-Onebit"
        self.Ring_oscillator_data(RO_data, Temperature, filer_name, dose)
        removecontent("output_data")


        self.set_trim(trimvoltage_dict["Vtrim_digital_-twobit"])   
        scan = RingOscTest(record_chip_status=False)
        scan.start(**configuration)
        RO_data, Temperature  = scan.analyze()
        scan.close()
        filer_name = "Vtrim_digital_-twobit"
        self.Ring_oscillator_data(RO_data, Temperature, filer_name, dose)
        self.set_trim(trimvoltage_dict["Vtrim_digital_optimal"])
        removecontent("output_data")   



if __name__ == "__main__":
    a = RingOscTest(record_chip_status=False)
    dose = 0
    a.main(dose)
  





