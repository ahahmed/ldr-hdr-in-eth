import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
from matplotlib.pyplot import figure
import basil
import visa
from matplotlib.dates import YearLocator, MonthLocator,HourLocator, DayLocator ,WeekdayLocator ,DateFormatter
import matplotlib.pyplot as plt
from operator import itemgetter
import ast
import sys, termios, tty, os, time
import csv
import matplotlib.dates as mdate
import calendar
import tables as tb
from bitarray import bitarray
from datetime import datetime
from bdaq53.fifo_readout import FifoReadout
from Ldr_Temp_monitor import meas_temperature
import logging
import matplotlib.pyplot as plt
import re

def list_folders(directory_ringoscillators):
	dir_array = os.listdir(directory_ringoscillators)
	return dir_array

def get_elements_line(x):
	array = []
	for i in range(len(x.split('	'))):
		a = x.split('	')[i]
		if a != '	':
			array.append(a)
			# print array
			# raw_input("a")
	return array




def get_ring_vector(dir_array,directory_ringoscillators):
	r0 = []
	r1 = []
	r2 = []
	r3 = []
	r4 = []
	r5 = []
	r6 = []
	r7 = []
	time = []
	idcount = []
	temperature = []
	global_pulse = 12 #value for global pulse equal to 409.6
	#TODO if global pulse is changed, change also the duration variable
	#global_pulse = 12 #value for global pulse 8 equal to 3276.8e-9
	global_pulse_duration = 3276.8e-9
	# time = 0
	j = 0
	# [int(s) for s in re.findall(r'\b\d+\b', dir_array)]
	for directory in dir_array:
		#Run2018-06-13 21_16_45
		#time.append(re.search(r'\d+-\d+-\d+ \d+_\d+_\d+', directory))


		idcount.append(j)
		j = j+1
		dir_textfile = directory_ringoscillators + '/' + directory+'/output_data.dat'
		if os.path.exists(dir_textfile):
			f = open(dir_textfile, 'r')
			lines = f.readlines()
			for i, x in enumerate(lines):
				try:
					if i == global_pulse:
						counts = get_elements_line(x)
						r0.append(float(counts[1])/(global_pulse_duration))
						r1.append(float(counts[2])/(global_pulse_duration))
						r2.append(float(counts[3])/(global_pulse_duration))
						r3.append(float(counts[4])/(global_pulse_duration))
						r4.append(float(counts[5])/(global_pulse_duration))
						r5.append(float(counts[6])/(global_pulse_duration))
						r6.append(float(counts[7])/(global_pulse_duration))
						r7.append(float(counts[8])/(global_pulse_duration))
						# print r0
					else:
						pass
				except ValueError,e:
					print "error",e,"on line",i,x
				#print x
				#raw_input("enter1")
				else:
					a = 0
	
	f.close()
	r0_ = []
	r1_ = []
	r2_ = []
	r3_ = []
	r4_ = []
	r5_ = []
	r6_ = []
	r7_ = []


	for i in range(len(r0)):
		r0_.append((r0[i]/r0[0])*100) 
		r1_.append((r1[i]/r1[0])*100) 
		r2_.append((r2[i]/r2[0])*100) 
		r3_.append((r3[i]/r3[0])*100) 
		r4_.append((r4[i]/r4[0])*100) 
		r5_.append((r5[i]/r5[0])*100) 
		r6_.append((r6[i]/r6[0])*100)
		r7_.append((r7[i]/r7[0])*100)  
	ring = [r0_,r1_,r2_,r3_,r4_,r5_,r6_,r7_]

	for i in range(0, len(r0_)):
		time.append(i)

	return ring, idcount, time , counts, temperature



now = time.time()
## Plot
def plot_ro(ring,directory_ringoscillators,directory_plots,idcount, time):

	global_pulse_duration = 204.8e-9


	line_color = ['#ffcc00','#ff6633','#ff0066','#cc33ff', '#3333ff', '#0099ff', '#33cc66', '#99cc33','#11cc55']
	legend = ['R0','R1','R2','R3','R4','R5','R6','R7']
	#figure(figsize=(15, 8))
	#fig, ax = plt.subplots(figsize=(15, 8))

	fig = plt.figure()
	ax = fig.add_subplot(111)


	print len(ring[0])
	print len(time)
	raw_input("2")

	#for i in range(len(ring)):
	ax.scatter(time, ring[0])#, label = legend[i],s=0.5, color = line_color[i])



	fig.legend(loc=5)
	plt.title('Ring Oscillators Frequency | global_pulse = 13 | LDO, 1.9 VDDD and VDDA')
	ax.set_xlabel('Time')
	ax.set_ylabel('Percentage')
	fig.savefig(directory_plots+'/'+'_ringOscillators_freq_last_plot.png', dpi=1000)
	#ax1.set_ylim(0, 100)
	#ax.set_ylim(-20,100)
	#plt.show()
	#ax.set_ylim(-20,100)
	plt.show()


# def save_RO_data_T(dir,counts):
# 	NTC = meas_temperature()
# 	counts.insert(0, time.time())
# 	with open(dir + '/' + 'RO.txt', 'a') as output:
# 		output.write(','.join(map(repr, counts))
# 		output.write("\n")








if __name__ == "__main__": 
    '''
trimvoltage_dict = "Vtrim_digital_optimal"
trimvoltage_dict = "Vtrim_digital_+Onebit"
trimvoltage_dict = "Vtrim_digital_+twobits"
trimvoltage_dict = "Vtrim_digital_+Threebits"
trimvoltage_dict = "Vtrim_digital_-Onebit"
trimvoltage_dict = "Vtrim_digital_-twobit"

    '''

    directory = '00_TEST_INFO'
    if not os.path.exists(directory):
	os.makedirs(directory)
    '''
    directory_plots = '00_TEST_INFO/ring_oscillator_processed '
    if not os.path.exists(directory_plots):
	os.makedirs(directory_plots)
    '''

    "1"		

    directory_ringoscillators = '00_TEST_INFO/RO/ring_oscillators/until10Mrad'
    dir_array = list_folders(directory_ringoscillators)
    directory_plots = '00_TEST_INFO/ring_oscillator_processed/RO_roomtemp'
    if not os.path.exists(directory_plots):
		os.makedirs(directory_plots)
    ring, idcount, time, counts, temperature = get_ring_vector(dir_array,directory_ringoscillators)
    datetime = []
    timeplot = []
    # for i in range(0,len(time)):
    # 	datetime.append(mdate.epoch2num(float(time[i][0])))
    # for i in range(0,len(time)):
    # 	timeplot.append((float(time[i][0])))
    # temp = []
    # for x in temperature:
    # 	for y in x:
    # 		temp.append(float(y))
    plot_ro(ring,directory_ringoscillators,directory_plots,idcount, time)
    
