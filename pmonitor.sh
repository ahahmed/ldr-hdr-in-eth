# #!/bin/bash 
# #######################################################################################
# #Script Name    :pmonistor.sh
# #Description    :It is used during the irradiation campaign to monitor and report system info via email 
# #Author         :Ahmed Abdirashid 
# #######################################################################################



Range='50' # %
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
MAILTO='LDRC2019@gmail.com' # functining email 
SUBJECT="$Low dose rate irradiation system information"

Range='70' # %
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
MAILTO='ldr2019zg@gmail.com' # functining email 
SUBJECT="Low dose rate irradiation system information"
MAILX='mailx'





######################## check if mailx is exeitng  ##################
which $MAILX > /dev/null 2>&1

if ! [ $? -eq 0 ]
then
          echo "Mailx is not installed"
          exit 1
fi


######################### used space in the computer ##################


USED=`df . | awk '{print $5}' | sed -ne 2p | cut -d"%" -f1`  
# get total free memory size in megabytes(MB) 
free=$(free -mt | grep Total | awk '{print $4}')	 
if [ $USED -gt $Range ]
#If used space is bigger than LIMIT
then
		echo "test"
		ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head > sysinfo

		echo "Used space in %" $USED,   "FREE sapce in MP" $free, "date +%Y%m%d_%H%M%S" > FILE2
		echo | mail -a "sysinfo" -a "memory_file" -v -s "computer" MAILTO # Send E-mail if used memory is above limit

		echo "Used space in %" $USED,   "FREE sapce in m" $free, "date +%Y%m%d_%H%M%S" > memory_file
		echo | mail -a "sysinfo" -a "memory_file" -v -s "computer" $MAILTO # Send E-mail if used memory is above limit

fi


########################## --  Supervsior monitor --- ##################
'''
Supervsior_monitor='Supervisor'

if ps ax | grep -v grep | grep $SERVICE > /dev/null
then
        echo "$SERVICE service is running"
else
        echo "Supervsior is down" | mail -a "sysinfo" -a "memory_file" -v -s "Supervsior monitor" MAILTO # Send E-mail if the supervsior is down

if ps ax | grep -v grep | grep $Supervsior_monitor > /dev/null
then
        echo "$SERVICE service is running"
	#echo "Supervsior is runnig" | mail -a "sysinfo" -a "memory_file" -v -s "Supervsior monitor" $MAILTO # Send E-mail if the supervsior is down
else
        echo "Supervsior is down" | mail -a "sysinfo" -a "memory_file" -v -s "Supervsior monitor" $MAILTO # Send E-mail if the supervsior is down

fi

'''
########################## Back up #####################################
#Purpose = LDR
#START
TIME=`date +%b-%d-%y%s`
FILENAME=backup_LDR.tar.gz 
SRCDIR=/home/lowdoserate4/projectdir/test_software_ldr/00_TEST_INFO  # Location of Important Data Directory (Source of backup).

tar --listed-incremental=/LDRV1/snapshot.file -czvf $FILENAME $SRCDIR
                         
#tar --listed-incremental=/LDRV1/snapshot.file -czvf /home/lowdoseraterd53/home-backup-'date +%d-%m-%Y'.tar /home/lowdoseraterd53/LDRV1

                         

