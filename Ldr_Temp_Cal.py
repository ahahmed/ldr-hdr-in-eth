from bdaq53.rd53a import RD53A
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.fifo_readout import FifoReadout
from Ldr_Temp_readout import Temperature_readout
import matplotlib.dates as mdate
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.dates import YearLocator, MonthLocator,HourLocator, DayLocator , DateFormatter
import matplotlib.pyplot as plt
#from bdaq53.serial_com import serial_com
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import pytemperature
import time
import os
import math
import datetime
import sys
import calendar
import numpy as np
import logging
import yaml
import time
import re
import csv
from matplotlib import pyplot as plt
from Ldr_Temp_monitor import meas_temperature

# variables/lists
raw_adc = []
averages = []
Sensors  = []
Filterout_numbers = [1,2,3,4]# for filtering out umwanted char
SenseArraysize = 16
d_config = 0

class Temperature_readout():
    def __init__(self,**kwargs):
        self.chip=RD53A()
        #self.chip.power_on(**kwargs)
        self.chip.init()
        self.fifo_readout = FifoReadout(self.chip)
        self.chip.init_communication()
        self.chip.set_dacs(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
 #       self.set_monitor()
	self.chip.enable_core_col_clock(range(50))   # here we can disable clock for a part of the array

    def set_monitor(self, d_config=0):

########       set SENSOR_CONFIG_X default values - the same config for the 4 sensors
        self.chip.write_register(register='SENSOR_CONFIG_0', data=(d_config << 6|d_config), write=True)
        self.chip.write_register(register='SENSOR_CONFIG_1', data=(d_config << 6|d_config), write=True)

    def get_temperature(self,sensor):
        temperatures={}
        try:
            timeout=10000

            self.chip.get_ADC(typ='U' ,address=sensor)

            for _ in range(timeout):
                if self.chip['FIFO'].get_FIFO_SIZE() > 0:
                    userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(self.chip['FIFO'].get_data()))
                    if len(userk_data)>0:
                        temperatures[sensor] = userk_data['Data'][0]
                        return temperatures
            else:
                logging.error('Timeout while waiting for chip Temperature.')
        except:
            logging.error('There was an error while receiving the chip status.')


    def scan(self,**kwargs):
	SENS_CONF_L  = 0b100000
	SENS_CONF_H  = 0b100001
        config_array=[SENS_CONF_L,SENS_CONF_H]
	s_config = 0
	DEM = 0
	CONFIG = 0
########       set MON_ENABLE = 1 (MONITOR_CONFIG[11])
########       set MON_BG_TRIM=10000 (MONITOR_CONFIG[10:6])
########       set MON_ADC_TRIM = 000101 (MONITOR_CONFIG[5:0])
        self.chip.write_register(register='MONITOR_CONFIG', data=0b110000000101, write=True)
        sensor_array=['TEMPSENS_1','TEMPSENS_2','TEMPSENS_3','TEMPSENS_4','RADSENS_1','RADSENS_2','RADSENS_3','RADSENS_4']
        raw_adc = []

# For each sensor, set SENS_SEL_BIAS to 0 and do ADC conversion for SENS_DEM varying from 0 to 15
        for sensor in sensor_array:
          for s_config in config_array:
            for DEM in range (0,16):
                REGCONF = (s_config | DEM << 1 )
                self.set_monitor(REGCONF)
                # print ("s_config is %d" %(s_config))
                # print ("DEM value is %d" % (DEM))
                # print ("REGCONF is %d" % (REGCONF))
                time.sleep (0)
                ADC =  self.get_temperature(sensor)
                print ADC
                raw_adc.append(ADC)

# Serial Communication for Keithley 2270 - Uncomment the following lines to use it
                #ser.write2tty('*RST')
                #ser.write2tty('MEAS:VOLT? 1, 0.000001')
                #out= ser.readFromtty()
        return str(raw_adc)

#############################################
# Calibrate data
#############################################

    def DeltaV_temperature(self, raw_adc):
        raw_adc_buffer = []
        raw_adc_buffer.append(re.findall(r'-?\d+\.?\d*', raw_adc)); raw_adc_buffer = raw_adc_buffer[0] # remove letter
        raw_adc_buffer = map(int, raw_adc_buffer) # find average of alle the ADC for sens 1
        for i in raw_adc_buffer:
            if i in Filterout_numbers:
                raw_adc_buffer.remove(i) # remove all 1,2,3,4
            chunks = [raw_adc_buffer[x:x+16] for x in xrange(0, len(raw_adc_buffer), 16)] # Devide the raw adc values to chunks of 16. Each sensor consis of 32 adc values
        for sublist in chunks: # avarage for the dynamic element method, length of the list = 16
            averages.append(sum(sublist)/float(len(sublist)))
            k = 0
            j = 1
        for i in range(len(averages)-1):# finding DeltaV for every sensor, lenth of the list = 8
            while k|j < len(averages):
                Sensors.append(averages[j]-averages[k])
                k = k+2
                j = j+2
        return Sensors


if __name__ == "__main__":
    #NTC measurements for calibration
    NTC = meas_temperature()
    TempLDR_instance = Temperature_readout()
    raw_adc = TempLDR_instance.scan()
    sensors = TempLDR_instance.DeltaV_temperature(raw_adc)

    # Calibration parameters
    k = 1.38064852*math.pow(10,-23) #
    q = 1.60217662*math.pow(10,-19) #
    V_refADC_SCC203 = 0.911 #[V]    #
    LSB = V_refADC_SCC203/math.pow(2, 12) #
    R = np.log(15)
    NTC_absolut = pytemperature.c2k(NTC)

    # Calculation Nf for each sensor
    Nf_sensors = [(i * LSB)/(((k*NTC_absolut)/(q))*R) for i in sensors]
    # sensors.insert(0,ntc)
    # sensors.insert(0, time.time())

    # Save calibration data to a file (Nf)
    chipid = raw_input("chip_id: ")
    directory = '00_TEST_INFO/temperature_sensor/Calibration_Nf_dir'
    if not os.path.exists(directory):
       		os.makedirs(directory)
    with open(directory + "/" + chipid +'.txt', 'w') as f:
        for item in Nf_sensors:
            print >> f, item
