import visa
import time
from operator import itemgetter
import ast
import sys, termios, tty, os, time
import csv
from datetime import date
import datetime
import numpy as np
import datetime
import time
import shutil
moment=time.strftime("%Y-%b-%d-%H-%M",time.localtime())

'''
Control instrument TTi PL303QMD -P Quad -mode Dual Power Supply 

Default: LDO mode
'''
class PSU(object):

    def __init__(self,**kwargs):
    	self.set_volt_VDDA = "1.9"
    	self.set_volt_VDDD = "1.9"
    	self.set_Current_VDDD = "1.3"
    	self.set_Current_VDDA = "1.3"
    	self.rm = visa.ResourceManager('@py')

    def wait(self, sec = 1):
    	time.sleep(sec)


    def power_config(self):
    	self.inst2 = self.rm.open_resource('ASRL/dev/ttyACM0::INSTR')
    	self.inst2.write("OPALL 0")
    	print 'power cycling...'
    	self.wait(1)
    	print("----- LDO mode ----- ")
    	print self.set_volt_VDDD
    	print self.set_volt_VDDA
    	print self.set_Current_VDDD
    	print self.set_Current_VDDA
    	print("-------------------- ")
    	'''Set voltage limit and current limit'''
    	self.inst2.write("V2 "+self.set_volt_VDDA)
    	self.inst2.write("V1 "+self.set_volt_VDDD)
    	self.inst2.write("I1 "+self.set_Current_VDDD)
    	self.inst2.write("I2 "+self.set_Current_VDDA)
    	self.wait(1)
    	print 'Power cycle done'
    	self.inst2.write("OPALL 1")

if __name__ == "__main__":
	powercontrol_instance = PSU()
	powercontrol_instance.power_config()

