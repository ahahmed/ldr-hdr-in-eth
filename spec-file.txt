# This file may be used to create an environment using:
# $ conda create --name <env> --file <this file>
# platform: linux-64
@EXPLICIT
https://repo.anaconda.com/pkgs/main/linux-64/conda-env-2.6.0-1.tar.bz2
https://repo.anaconda.com/pkgs/free/linux-64/blas-1.0-mkl.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/ca-certificates-2019.1.23-0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/intel-openmp-2019.1-144.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/libffi-3.2.1-hd88cf55_4.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/libgcc-ng-8.2.0-hdf63c60_1.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/libgfortran-ng-7.3.0-hdf63c60_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/libstdcxx-ng-8.2.0-hdf63c60_1.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/yaml-0.1.7-had09818_2.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/bzip2-1.0.6-h14c3975_5.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/expat-2.2.6-he6710b0_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/icu-58.2-h9c2bf20_1.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/jpeg-9b-h024ee3a_2.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/libuuid-1.0.3-h1bed415_2.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/libxcb-1.13-h1bed415_1.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/lzo-2.10-h49e0be7_2.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/mkl-2019.1-144.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/ncurses-6.1-he6710b0_1.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/openssl-1.1.1a-h7b6447c_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/pcre-8.42-h439df22_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/snappy-1.1.7-hbae5bb6_3.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/xz-5.2.4-h14c3975_4.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/zlib-1.2.11-h7b6447c_3.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/blosc-1.15.0-hd408876_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/glib-2.56.2-hd408876_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/hdf5-1.10.4-hb1b8bf9_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/libedit-3.1.20170329-h6b74fdf_2.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/libpng-1.6.36-hbc83047_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/libxml2-2.9.9-he19cac6_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/readline-7.0-h7b6447c_5.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/tk-8.6.8-hbc83047_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/dbus-1.13.6-h746ee38_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/freetype-2.9.1-h8a8886c_1.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/gstreamer-1.14.0-hb453b48_1.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/sqlite-3.26.0-h7b6447c_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/fontconfig-2.13.0-h9420a91_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/gst-plugins-base-1.14.0-hbbd80ab_1.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/python-2.7.15-h9bab390_6.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/asn1crypto-0.24.0-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/atomicwrites-1.2.1-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/attrs-18.2.0-py27h28b3542_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/backports-1.0-py27_1.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/backports_abc-0.5-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/bitarray-0.8.3-py27h14c3975_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/certifi-2018.11.29-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/chardet-3.0.4-py27_1.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/enum34-1.1.6-py27_1.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/funcsigs-1.0.2-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/functools32-3.2.3.2-py27_1.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/futures-3.2.0-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/idna-2.8-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/ipaddress-1.0.22-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/kiwisolver-1.0.1-py27hf484d3e_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/numpy-base-1.15.4-py27hde5b4d6_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/pluggy-0.8.1-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/free/linux-64/progressbar-2.3-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/py-1.7.0-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/pycosat-0.6.3-py27h14c3975_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/pycparser-2.19-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/pyparsing-2.3.1-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/pysocks-1.6.8-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/pytz-2018.9-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/pyyaml-3.13-py27h14c3975_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/qt-5.9.7-h5867ecd_1.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/ruamel_yaml-0.15.46-py27h14c3975_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/scandir-1.9.0-py27h14c3975_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/sip-4.19.8-py27hf484d3e_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/six-1.12.0-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/subprocess32-3.5.3-py27h7b6447c_0.tar.bz2
https://repo.anaconda.com/pkgs/main/noarch/tqdm-4.29.1-py_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/cffi-1.11.5-py27he75722e_1.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/cycler-0.10.0-py27hc7354d3_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/llvmlite-0.27.0-py27hd408876_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/mkl_random-1.0.2-py27hd81dba3_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/more-itertools-5.0.0-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/pathlib2-2.3.3-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/pyqt-5.9.2-py27h05f1152_2.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/python-dateutil-2.7.5-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/setuptools-40.6.3-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/singledispatch-3.4.0.3-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/backports.functools_lru_cache-1.5-py27_1.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/cryptography-2.4.2-py27h1ba5d50_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/pytest-4.2.0-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/tornado-5.1.1-py27h7b6447c_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/wheel-0.32.3-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/pip-18.1-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/pyopenssl-18.0.0-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/urllib3-1.24.1-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/requests-2.21.0-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/conda-4.6.2-py27_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/matplotlib-2.2.3-py27hb69df0a_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/mkl_fft-1.0.10-py27ha843d7b_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/numpy-1.15.4-py27h7e9f1db_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/numba-0.42.0-py27h962f231_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/numexpr-2.6.9-py27h9e4a6bb_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/scipy-1.2.0-py27h7c811a0_0.tar.bz2
https://repo.anaconda.com/pkgs/main/linux-64/pytables-3.4.4-py27h71ec239_0.tar.bz2
