#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#
import tables as tb
from tqdm import tqdm
import sys, os, time
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting

from bdaq53.rd53a import RD53A
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.fifo_readout import FifoReadout
#from bdaq53.serial_com import serial_com
from datetime import datetime
import logging
import yaml
import time
import visa
import socket
import sys
import time
import re
import os



voltage_mux_array=['ADCbandgap','CAL_MED_left','CAL_HI_left','TEMPSENS_1','RADSENS_1','TEMPSENS_2','RADSENS_2','TEMPSENS_4','RADSENS_4','VREF_VDAC','VOUT_BG','IMUX_out','VCAL_MED','VCAL_HIGH','RADSENS_3','TEMPSENS_3','REF_KRUM_LIN','Vthreshold_LIN','VTH_SYNC','VBL_SYNC','VREF_KRUM_SYNC','VTH_HI_DIFF','VTH_LO_DIFF','VIN_Ana_SLDO','VOUT_Ana_SLDO','VREF_Ana_SLDO','VOFF_Ana_SLDO','ground','ground1','VIN_Dig_SLDO','VOUT_Dig_SLDO','VREF_Dig_SLDO','VOFF_Dig_SLDO','ground2']

current_mux_array=['Iref','IBIASP1_SYNC','IBIASP2_SYNC','IBIAS_DISC_SYNC','IBIAS_SF_SYNC','ICTRL_SYNCT_SYNC','IBIAS_KRUM_SYNC','COMP_LIN','FC_BIAS_LIN','KRUM_CURR_LIN','LDAC_LIN','PA_IN_BIAS_LIN','COMP_DIFF','PRECOMP_DIFF','FOL_DIFF','PRMP_DIFF','LCC_DIFF','VFF_DIFF','VTH1_DIFF','VTH2_DIFF','CDR_CP_IBIAS','VCO_BUFF_BIAS','VCO_IBIAS','CML_TAP_BIAS0','CML_TAP_BIAS1','CML_TAP_BIAS2']



'''
    This basic test scans over the values of the selected DAC and
    measures the resulting analog value with the chip's internal ADC.
'''

chip_configuration = 'default_chip.yaml'
##### monitor settings
##### default sensor config to 000000
#d_config = 0

local_configuration = {'MONITOR_CONFIG'  : 1096,
                       'SENSOR_CONFIG_0' : 4095,
                       'SENSOR_CONFIG_1' : 4095}



class ADC_readout(ScanBase):
    scan_id = "adc_scan"

    def open_multimeter_socket(self):
        rm = visa.ResourceManager('@py')
        time.sleep(3)
        print(rm.list_resources())
        raw_input("all the intrumetns")

        #inst = rm.open_resource('ASRL/dev/ttyUSB0::INSTR')
        inst2 = rm.open_resource('ASRL/dev/ttyUSB0::INSTR')
        print sockC.sendall("*IDN?")





    def get_multimeter_value(self):
	##### USER INPUT =====
    	current_measure_command = 'km'
        time_data = []
    	elapsed_data = []
    	c_data = []

        start = time.time()
    	message = current_measure_command
    	sockC.sendall(message)
    	msgReceivedC = sockC.recv(1024)
    	end = time.time()
    	_c = [float(x) for x in re.findall("-?\d+.?\d*(?:[Ee]-\d+)?", msgReceivedC)]
    	c_data.append(_c)
    	deltaT = int(end - start)
    	data = [time.strftime("%d-%m-%Y %H:%M:%S"), '\t', str(deltaT), '\t', str(_c[0]), '\n']
    	#print time.strftime("%d-%m-%Y %H:%M:%S") + '\t' + str(deltaT) + ' s \t' + str(_c[0]) + ' V'
    	print 'value: '
    	print str(_c[0])
    	#time.sleep(0.1)
        print "Scan complete..."
    	return str(_c[0])

        def close_multimeter_socket(self):
            print >> sys.stderr, 'closing socket'
    	sockC.close()



    def get_adc_value(self,mux_selection, typ):
        adc_value={}
	print mux_selection
        try:
            timeout=10000
            self.chip.get_ADC(typ ,address=mux_selection)
            for _ in range(timeout):
                if self.chip['FIFO'].get_FIFO_SIZE() > 0:
                    userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(self.chip['FIFO'].get_data()))
                    if len(userk_data)>0:
                        adc_value[mux_selection] = userk_data['Data'][0]
                        return adc_value[mux_selection]
            else:
                logging.error('Timeout while waiting for ADC measurement.')
        except:
            logging.error('There was an error while receiving the chip status.')


    def get_adc_external_signal_value(self, typ):
        adc_value={}
	#print mux_selection
        try:
            timeout=10000
            self.chip.get_ADC_external_signal(typ)
            for _ in range(timeout):
                if self.chip['FIFO'].get_FIFO_SIZE() > 0:
                    userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(self.chip['FIFO'].get_data()))
                    if len(userk_data)>0:
                        adc_value = userk_data['Data'][0]
                        return adc_value
            else:
                logging.error('Timeout while waiting for ADC measurement.')
        except:
            logging.error('There was an error while receiving the chip status.')


    def scan(self,**kwargs):
	# globalTime = sys.argv[1]
	# dose = sys.argv[2]

	#Multimeter communication:

	# Create a TCP/IP socket
        rm = visa.ResourceManager('@py')
        time.sleep(3)
        print(rm.list_resources())

        #inst = rm.open_resource('ASRL/dev/ttyUSB0::INSTR')
        inst2 = rm.open_resource('ASRL/dev/ttyUSB0::INSTR')
        #inst2.query("*IDN?")
        print (inst2.query("*IDN?"))
        raw_input("ask")






	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)

	directory = directory + '/adc_measurements'
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	if not os.path.exists(directory):
    		os.makedirs(directory)

		out_v = open( directory + '/'+"ACCUMULATIVE_adc_v.txt", "a")
		tobewritten = "%Timestamp  T_NTC(C) GlobalTime(s)  DoseRange(Mrad) ADCbandgap CAL_MED_left CAL_HI_left TEMPSENS_1 RADSENS_1 TEMPSENS_2 RADSENS_2 TEMPSENS_4 RADSENS_4 VREF_VDAC VOUT_BG IMUX_out VCAL_MED VCAL_HIGH RADSENS_3 TEMPSENS_3 REF_KRUM_LIN Vthreshold_LIN VTH_SYNC VBL_SYNC VREF_KRUM_SYNC VTH_HI_DIFF VTH_LO_DIFF VIN_Ana_SLDO VOUT_Ana_SLDO VREF_Ana_SLDO VOFF_Ana_SLDO ground ground1 VIN_Dig_SLDO VOUT_Dig_SLDO VREF_Dig_SLDO VOFF_Dig_SLDO ground2 External_signal\n"
		out_v.write(tobewritten)
		out_v.close()

		out_i = open( directory + '/'+"ACCUMULATIVE_adc_i.txt", "a")
		tobewritten = "%Timestamp  T_NTC(C) GlobalTime(s)  DoseRange(Mrad) Iref IBIASP1_SYNC IBIASP2_SYNC IBIAS_DISC_SYNC IBIAS_SF_SYNC ICTRL_SYNCT_SYNC IBIAS_KRUM_SYNC COMP_LIN FC_BIAS_LIN KRUM_CURR_LIN LDAC_LIN PA_IN_BIAS_LIN COMP_DIFF PRECOMP_DIFF FOL_DIFF PRMP_DIFF LCC_DIFF VFF_DIFF VTH1_DIFF VTH2_DIFF CDR_CP_IBIAS VCO_BUFF_BIAS VCO_IBIAS CML_TAP_BIAS0 CML_TAP_BIAS1 CML_TAP_BIAS2\n"
		out_i.write(tobewritten)
		out_i.close()


		out_v_multim = open( directory + '/'+"ACCUMULATIVE_multimeter_v.txt", "a")
		tobewritten = "%Timestamp  T_NTC(C) GlobalTime(s)  DoseRange(Mrad) ADCbandgap CAL_MED_left CAL_HI_left TEMPSENS_1 RADSENS_1 TEMPSENS_2 RADSENS_2 TEMPSENS_4 RADSENS_4 VREF_VDAC VOUT_BG IMUX_out VCAL_MED VCAL_HIGH RADSENS_3 TEMPSENS_3 REF_KRUM_LIN Vthreshold_LIN VTH_SYNC VBL_SYNC VREF_KRUM_SYNC VTH_HI_DIFF VTH_LO_DIFF VIN_Ana_SLDO VOUT_Ana_SLDO VREF_Ana_SLDO VOFF_Ana_SLDO ground ground1 VIN_Dig_SLDO VOUT_Dig_SLDO VREF_Dig_SLDO VOFF_Dig_SLDO ground2 External_signal\n"
		out_v_multim.write(tobewritten)
		out_v_multim.close()


		out_i_multim = open( directory + '/'+"ACCUMULATIVE_multimeter_i.txt", "a")
		tobewritten = "%Timestamp  T_NTC(C) GlobalTime(s)  DoseRange(Mrad) Iref IBIASP1_SYNC IBIASP2_SYNC IBIAS_DISC_SYNC IBIAS_SF_SYNC ICTRL_SYNCT_SYNC IBIAS_KRUM_SYNC COMP_LIN FC_BIAS_LIN KRUM_CURR_LIN LDAC_LIN PA_IN_BIAS_LIN COMP_DIFF PRECOMP_DIFF FOL_DIFF PRMP_DIFF LCC_DIFF VFF_DIFF VTH1_DIFF VTH2_DIFF CDR_CP_IBIAS VCO_BUFF_BIAS VCO_IBIAS CML_TAP_BIAS0 CML_TAP_BIAS1 CML_TAP_BIAS2\n"
		out_i_multim.write(tobewritten)
		out_i_multim.close()


	directory_textfile_v = directory + '/'+"ACCUMULATIVE_adc_v.txt"
	directory_textfile_i = directory + '/'+"ACCUMULATIVE_adc_i.txt"
	directory_textfile_i_multim = directory + '/'+"ACCUMULATIVE_multimeter_i.txt"
	directory_textfile_v_multim = directory + '/'+"ACCUMULATIVE_multimeter_v.txt"
	directory_notextfile = directory


	out_v = open( directory + '/'+"ACCUMULATIVE_adc_v.txt", "a")
	out_v_multim = open( directory + '/'+"ACCUMULATIVE_multimeter_v.txt", "a")
	timestamp = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
	T_FMC = self.chip._measure_temperature_ntc_CERNFMC()
	tobewritten =  str(timestamp) + " " + str("%.4f" %T_FMC) + " " + str(time.time()-float(globalTime))+ ' ' + dose + ' '
	tobewritten_multim = tobewritten

	#This I only do it to initialize the ADC, cause sometimes the first value given is 'None' and I don't solve it...
	self.get_adc_value('ADCbandgap', 'U')
        for mux_selection in voltage_mux_array:
            ADC =  self.get_adc_value(mux_selection, 'U')

	    sockC.sendall(message)
	    msgReceivedC = sockC.recv(1024)
	    end = time.time()
	    _c = [float(x) for x in re.findall("-?\d+.?\d*(?:[Ee]-\d+)?", msgReceivedC)]
	    multimeter = str(_c[0])

	    tobewritten = tobewritten + str(ADC) + ' '
	    tobewritten_multim = tobewritten_multim + str(multimeter) + ' '

	#for external signal with adc
	ADC = self.get_adc_external_signal_value('U')
	tobewritten = tobewritten + str(ADC) + ' '
	tobewritten = tobewritten + "\n"
	tobewritten_multim = tobewritten_multim + "\n"

	out_v.write(tobewritten)
        out_v.close()
	out_v_multim.write(tobewritten_multim)
        out_v_multim.close()

	out_i = open( directory + '/'+"ACCUMULATIVE_adc_i.txt", "a")
	out_i_multim = open( directory + '/'+"ACCUMULATIVE_multimeter_i.txt", "a")

	timestamp = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
	T_FMC = self.chip._measure_temperature_ntc_CERNFMC()
	tobewritten =  str(timestamp) + " " + str("%.4f" %T_FMC) + " " + str(time.time()-float(globalTime))+ ' ' + dose + ' '
	tobewritten_multim = tobewritten


        for mux_selection in current_mux_array:
	    ADC =  self.get_adc_value(mux_selection, 'I')

	    sockC.sendall(message)
	    msgReceivedC = sockC.recv(1024)
	    end = time.time()
	    _c = [float(x) for x in re.findall("-?\d+.?\d*(?:[Ee]-\d+)?", msgReceivedC)]
	    multimeter = str(_c[0])

	    #multimeter = self.get_multimeter_value() #for debugging, here set the measurement taken.
	    tobewritten = tobewritten + str(ADC) + ' '
	    tobewritten_multim = tobewritten_multim + str(multimeter) + ' '
	    #raw_input('Press to continue')
	tobewritten = tobewritten + "\n"
	tobewritten_multim = tobewritten_multim + "\n"
	out_i.write(tobewritten)
        out_i.close()
	out_i_multim.write(tobewritten_multim)
        out_i_multim.close()

	print >> sys.stderr, 'closing socket'
	sockC.close()



if __name__ == "__main__":
    scan = ADC_readout()
    scan.start(**local_configuration)
    scan.close()
