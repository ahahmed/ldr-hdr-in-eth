# Low dose rate cold setup

## Do these commands in following order
```
curl https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh -o miniconda.sh
bash miniconda.sh -b -p $HOME/miniconda
```


## Open the bashrc file
```
gedit ~/.bashrc
```

## Add this line bashrc: export PATH=$HOME/miniconda/bin:$PATH

```
export PATH=$HOME/miniconda/bin:$PATH
```




```
conda update --yes conda
conda install --yes numpy bitarray pytest pyyaml progressbar scipy numba pytables matplotlib tqdm
pip install cocotb
pip install pytemperature
pip install pyvisa
```


## Install bdaq53

```
git clone -b v2.4.13 https://github.com/SiLab-Bonn/basil
cd basil

python setup.py develop
cd ..

git clone -b v0.9.0 https://gitlab.cern.ch/silab/bdaq53.git
cd bdaq53

python setup.py develop

git checkout -b development
```

## Configure your ip adress:

```
sudo ifconfig em1 192.168.10.2
```

## Test with ping if it works

```
ping 192.168.10.12
```


## Install mailx by following these guidelines
```
mailx instructions - https://gist.github.com/ilkereroglu/aa6c868153d1c5d57cd8
```
* [mailx](https://gist.github.com/ilkereroglu/aa6c868153d1c5d57cd8) 



```
pip install psutil

```

```
pip install pyvisa-py
```

```
pip install pyusb
```




## Links
* [Characterization procedure](https://docs.google.com/document/d/1OVvbMjzegAQ_8J8vgv8r5zGI19BDgaPcl616BQzRRBc/edit?usp=sharing) - The characterization steps used.
* [Characterization data](https://docs.google.com/spreadsheets/d/1Afamwy3At8myhvHy-8G2EweHnoXkRFD66-sNrlU75cI/edit?usp=sharing) - The parameters that are optimized and collected.




