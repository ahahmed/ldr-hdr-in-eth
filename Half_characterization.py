import time
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import os
import glob
import datetime
from bdaq53.fifo_readout import FifoReadout
import logging
import sys, select
import shutil
from shutil import copytree, ignore_patterns

import fnmatch
import os
import re



'''Optimal'''
#Trim_chip =  '0b0110010101'  #Chip0535
Trim_chip = '1001111001'   #Chip0575
#Trim_chip = '0b1010010011' #Chip333
#Trim_chip = '0b0110010101'
#Trim_chip = '0b0110010101'
#Trim_chip = '0b0110010101'
	
'''Trimbit for LINEAR FE'''
#Trim_chip_lin =   '0b0110011100'  #Chip0535
Trim_chip_lin = '1001111111'   #Chip0575
#Trim_chip_lin = '0b1010010011' #Chip333
#Trim_chip_lin = '0b0110010101'
#Trim_chip_lin = '0b0110010101'
#Trim_chip_lin = '0b0110010101'	





def removecontent(path):
	if os.path.exists(path):
		try:
			os.system("rm -r "+ path )
		except Exception as e:
			pass



def PowertrimLIN():
	os.system('python trimfunc.py --register="VOLTAGE_TRIM" --binary='+Trim_chip_lin)

def PowertrimObtimal():
	os.system('python trimfunc.py --register="VOLTAGE_TRIM" --binary=' +Trim_chip)


#It goes through all the defaul_chip files generated
def ChipCharact():
	directory = '00_TEST_INFO'+ '/' +'Half_Characterization'
	currentdir = os.getcwd()
	if not os.path.exists(directory):
    		os.makedirs(directory)


	t0 = time.time()
	timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
	testTag = ('Half_Characterization_during_irrad')
	#testTag = 'irradiation_'

	#testTag = 'AfterDoseOf_100Mrad_RoomT_'
	directory = directory + '/' + testTag + '_' + timestamp
	if not os.path.exists(directory):
		os.makedirs(directory)



	

	#Sync FE:
	try:
		os.rename('default_chip_sync.yaml','default_chip.yaml')
		removecontent("output_data")
		# Tuning the front end with auto zeoing, and width of 6, which are saved in their resepctive folders.
		os.system("bdaq53 scan_noise_occupancy_sync")
		os.system("bdaq53 scan_threshold_width6")
		shutil.move("output_data", directory + '/SYNC/sync_threshold_sync_scan_width6')
		os.rename('default_chip.yaml','default_chip_sync.yaml')

	except Exception as e:
		raise
	finally:
		pass
	

	#Lin FE:
	try:	
		os.rename('default_chip_lin.yaml','default_chip.yaml')
		PowertrimLIN() # Trimming DAC for linear front end monitoring
		'''
		Make two copies of Tdac DIFF mask files to ldr-software folder and call them: lin_Tdac_metatune_mask.h5  and 			lin_Tdac_metatune_mask_tune_ini.h5
		'''


		if not os.path.exists('output_data'):
			os.makedirs("output_data")

		shutil.copy("lin_Tdac_metatune_mask" + ".h5", "output_data")
		os.system("bdaq53 scan_noise_occupancy_lin")
		os.system("bdaq53 scan_threshold_lin")
		shutil.move("output_data", directory + '/LIN/lin_threshold_scan')
		os.rename('default_chip.yaml','default_chip_lin.yaml')
		PowertrimLIN() # Trimming DAC optimal VDDA and VDDD
	except Exception as e:
		raise
	finally:
		pass


	#Diff FE:
	try:
		os.rename('default_chip_diff.yaml','default_chip.yaml')


		'''
		Make two copies of Tdac DIFF mask files to ldr-software folder and call them: 'diff_Tdac_metatune_mask.h5'  and 		'diff_Tdac_metatune_mask_tune_ini.h5'
		'''

		if not os.path.exists('output_data'):
			os.makedirs("output_data")


					
		''' using linear triming TDACs before irradiation for a threshold scan: '''
		if not os.path.exists('output_data'):
			os.makedirs("output_data")

		shutil.copy("diff_Tdac_metatune_mask" + ".h5", "output_data")
		os.system("bdaq53 scan_noise_occupancy_diff")
		os.system("bdaq53 scan_threshold_diff")
		shutil.move("output_data", directory + '/DIFF/diff_threshold_scan')
		os.rename('default_chip.yaml','default_chip_diff.yaml')
	except Exception as e:
		raise
	finally:
		pass




	#Diff FE for 100 Mrad:
	try:
 	

		'''
		Make two copies of Tdac DIFF mask files to ldr-software folder and call them: 'diff_Tdac_metatune_mask.h5'  and 		'diff_Tdac_metatune_mask_tune_ini.h5'
		'''

		os.rename('default_chip_differential100Mrad.yaml','default_chip.yaml')			
		''' using linear triming TDACs before irradiation for a threshold scan: '''
		if not os.path.exists('output_data'):
			os.makedirs("output_data")

		shutil.copy("diff_Tdac_metatune_mask_100mrad" + ".h5", "output_data")
		os.system("bdaq53 scan_noise_occupancy_diff")
		os.system("bdaq53 scan_threshold_diff")
		shutil.move("output_data", directory + '/DIFF_100mrad/diff_threshold_scan')
		os.rename('default_chip.yaml','default_chip_differential100Mrad.yaml')
	except Exception as e:
		raise
	finally:
		pass







	#
	# tend = time.time()
	# print 'Total time needed: ', tend-t0

if __name__ == '__main__':
	 ChipCharact()
