#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Wafer probing script to probe a full wafer
'''
import tables as tb
from tqdm import tqdm
import sys, os, time
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting

from bdaq53.rd53a import RD53A
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.fifo_readout import FifoReadout
#from bdaq53.serial_com import serial_com
from datetime import datetime
import logging
import yaml
import time

import socket
import sys
import time
import re
import os
import math
import os
import time
import yaml
import logging
import copy
import numpy as np

from basil.dut import Dut

local_configuration = {'MONITOR_CONFIG'  : 1096,
                       'SENSOR_CONFIG_0' : 4095,
                       'SENSOR_CONFIG_1' : 4095}


V_refADC = 0.811 #[V] #
r = 10000
LSB = float(V_refADC/math.pow(2, 12)) 
legend_V='Time_stamp ADCbandgap CAL_MED_left CAL_HI_left TEMPSENS_1 RADSENS_1 TEMPSENS_2 RADSENS_2 TEMPSENS_4 RADSENS_4 VREF_VDAC VOUT_BG IMUX_out VCAL_MED VCAL_HIGH RADSENS_3 TEMPSENS_3 REF_KRUM_LIN Vthreshold_LIN VTH_SYNC VBL_SYNC VREF_KRUM_SYNC VTH_HI_DIFF VTH_LO_DIFF VIN_Ana_SLDO VOUT_Ana_SLDO VREF_Ana_SLDO VOFF_Ana_SLDO ground ground1 VIN_Dig_SLDO VOUT_Dig_SLDO VREF_Dig_SLDO VOFF_Dig_SLDO ground2'
legend_I='Time_stamp Iref IBIASP1_SYNC IBIASP2_SYNC IBIAS_DISC_SYNC IBIAS_SF_SYNC ICTRL_SYNCT_SYNC IBIAS_KRUM_SYNC COMP_LIN FC_BIAS_LIN KRUM_CURR_LIN LDAC_LIN PA_IN_BIAS_LIN COMP_DIFF PRECOMP_DIFF FOL_DIFF PRMP_DIFF LCC_DIFF VFF_DIFF VTH1_DIFF VTH2_DIFF CDR_CP_IBIAS VCO_BUFF_BIAS VCO_IBIAS CML_TAP_BIAS0 CML_TAP_BIAS1 CML_TAP_BIAS2'


def array2string(array):
	string = ''
	for i in array:
	    string = string + ' ' + (str(i))
	return string


class ADC_scan_multimeter(object):

    def __init__(self, conf='default_chip.yaml'):
        self.config_file = conf
        self.logger = logging.getLogger('test')
        self.logger.success = lambda msg, *args, **kwargs: self.logger.log(logging.SUCCESS, msg, *args, **kwargs)
        self.init()
        self.chip=RD53A()
        self.chip.init()
        self.fifo_readout = FifoReadout(self.chip)
        self.chip.init_communication()
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
	self.chip.enable_core_col_clock(range(50))   # here we can disable clock for a part of the array

    def init(self):
        devices = Dut('keithley2410LDR.yaml')
        devices.init()
        self.smu = devices['Sourcemeter']

    def measure_voltage(self):
        try:       
            value = self.smu.get_voltage()
            value = self.smu.get_voltage().split(',')[0]
        except ValueError as e:
            self.logger.exception(e)
            value = 0.0
        time.sleep(0.1)
 
        return float(value)


    def get_adc_mux_multimeter(self, timeout=1000):

	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)

	directory = directory + '/ADC_MULTIMETER'	
	if not os.path.exists(directory):
    		os.makedirs(directory)

        name_file_adc_v = 'ACCUMULATIVE_adc_v.txt'
        name_file_adc_i = 'ACCUMULATIVE_adc_i.txt'
        name_file_multimeter_v = 'ACCUMULATIVE_multimeter_v.txt'
        name_file_multimeter_i = 'ACCUMULATIVE_multimeter_i.txt'



	exists = os.path.isfile(directory + '/' + name_file_adc_v)
	if not(exists):
		out_adc_v = open( directory + '/'+name_file_adc_v, "a")	
		tobewritten = legend_V + "\n"
		out_adc_v.write(tobewritten) 
		out_adc_v.close()

	exists = os.path.isfile(directory + '/' + name_file_adc_i)
	if not(exists):
		out_adc_i = open( directory + '/'+name_file_adc_i, "a")	
		tobewritten = legend_I + "\n"
		out_adc_i.write(tobewritten) 
		out_adc_i.close()


	exists = os.path.isfile(directory + '/' + name_file_multimeter_v)
	if not(exists):
		out_multimeter_v = open( directory + '/'+name_file_multimeter_v, "a")	
		tobewritten = legend_V + "\n"
		out_multimeter_v.write(tobewritten) 
		out_multimeter_v.close()


	exists = os.path.isfile(directory + '/' + name_file_multimeter_i)
	if not(exists):
		out_multimeter_i = open( directory + '/'+name_file_multimeter_i, "a")	
		tobewritten = legend_I + "\n"
		out_multimeter_i.write(tobewritten) 
		out_multimeter_i.close()


        timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")


        

        voltages_internal = []
        currents_internal = []
        voltages_external = []
        currents_external = []

        self.logger.info('Recording chip status...')
        n = 1
        try:
            self.chip.enable_monitor_filter()
            self.chip.enable_monitor_data()
            #VOLTAGES IN TABLE 30
            for vmonitor in np.arange(0, 33, 1):
                self.chip.get_ADC(typ='U', address=vmonitor)
                name = self.chip.voltage_mux[vmonitor]
                for _ in range(timeout):
                    if self.chip['FIFO'].get_FIFO_SIZE() > 0:
                        userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(self.chip['FIFO'].get_data()))
                        v_adc = LSB*(userk_data['Data'][0])
                        voltages_internal.append(v_adc)
                        v_ext = self.measure_voltage()
                        voltages_external.append(v_ext)                       
		        print('*******************')
                        print 'Vmonitor value: ', vmonitor
		        print(name)
			print 'Voltage measured with multimeter: ', v_ext
			print 'Voltage measured with internal ADC : ', v_adc
		        print '*******************'
                        #raw_input("enter")
                        if len(userk_data) > 0:
                            break
                else:
                    self.logger.error('Timeout while waiting for chip status.')

            #CURRENTS IN TABLE 29
            for cmonitor in np.	arange(0, 26, 1):
                self.chip.get_ADC(typ='I', address=cmonitor)
                name = self.chip.current_mux[cmonitor]
                for _ in range(timeout):
                    if self.chip['FIFO'].get_FIFO_SIZE() > 0:
                        userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(self.chip['FIFO'].get_data()))
                        i_adc = LSB*(userk_data['Data'][0])
                        currents_internal.append(i_adc)
                        v_ext = self.measure_voltage()         
                        currents_external.append(v_ext)
		        print'*******************'
                        print 'Cmonitor value: ', cmonitor
		        print(name)
			print 'Voltage measured with multimeter: ', v_ext 
			print 'Voltage measured with internal ADC : ', i_adc
		        print '*******************'
                        #raw_input("enter")
                        if len(userk_data) > 0:
                            break
                else:
                    self.logger.error('Timeout while waiting for chip status.')
        except:
            self.logger.error('There was an error while receiving the chip status.')
        self.chip._reset_ADC()
        #print voltages_internal, currents_internal, voltages_external, currents_external

	#Writting ADC voltage values:
	out_adc_v = open( directory + '/'+name_file_adc_v, "a")	
	tobewritten = timestamp + array2string(voltages_internal) +  "\n"
	out_adc_v.write(tobewritten) 
	out_adc_v.close()

	#Writting ADC current values:
	out_adc_i = open( directory + '/'+name_file_adc_i, "a")	
	tobewritten = timestamp + array2string(currents_internal) + "\n"
	out_adc_i.write(tobewritten) 
	out_adc_i.close()

	#Writting Multimeter voltage values:
	out_multimeter_v = open( directory + '/'+name_file_multimeter_v, "a")	
	tobewritten = timestamp + array2string(voltages_external) + "\n"
	out_multimeter_v.write(tobewritten) 
	out_multimeter_v.close()

	#Writting Multimeter current values:
	out_multimeter_i = open( directory + '/'+name_file_multimeter_i, "a")	
	tobewritten = timestamp + array2string(currents_external) + "\n"
	out_multimeter_i.write(tobewritten) 
	out_multimeter_i.close()

        return voltages_internal, currents_internal, voltages_external, currents_external



    def main(self, **kwargs):
        #for x in range(1):
        voltages_internal, currents_internal, voltages_external, currents_external = self.get_adc_mux_multimeter()



class ADC_scan(object):

    def __init__(self, conf='default_chip.yaml'):
        self.config_file = conf
        self.logger = logging.getLogger('test')
        self.logger.success = lambda msg, *args, **kwargs: self.logger.log(logging.SUCCESS, msg, *args, **kwargs)
        self.chip=RD53A()
        self.chip.init()
        self.fifo_readout = FifoReadout(self.chip)
        self.chip.init_communication()
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
	self.chip.enable_core_col_clock(range(50))   # here we can disable clock for a part of the array



    def get_adc_mux_multimeter(self, timeout=1000):

	directory = '00_TEST_INFO'
	if not os.path.exists(directory):
    		os.makedirs(directory)

	directory = directory + '/ADC_MULTIMETER'	
	if not os.path.exists(directory):
    		os.makedirs(directory)

        name_file_adc_v = 'ACCUMULATIVE_adc_v.txt'
        name_file_adc_i = 'ACCUMULATIVE_adc_i.txt'




	exists = os.path.isfile(directory + '/' + name_file_adc_v)
	if not(exists):
		out_adc_v = open( directory + '/'+name_file_adc_v, "a")	
		tobewritten = legend_V + "\n"
		out_adc_v.write(tobewritten) 
		out_adc_v.close()

	exists = os.path.isfile(directory + '/' + name_file_adc_i)
	if not(exists):
		out_adc_i = open( directory + '/'+name_file_adc_i, "a")	
		tobewritten = legend_I + "\n"
		out_adc_i.write(tobewritten) 
		out_adc_i.close()



        timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")


        voltages_internal = []
        currents_internal = []


        self.logger.info('Recording chip status...')
        n = 1
        try:
            self.chip.enable_monitor_filter()
            self.chip.enable_monitor_data()
            #VOLTAGES IN TABLE 30
            for vmonitor in np.arange(0, 33, 1):
                self.chip.get_ADC(typ='U', address=vmonitor)                
                name = self.chip.voltage_mux[vmonitor]
                for _ in range(timeout):
                    if self.chip['FIFO'].get_FIFO_SIZE() > 0:
                        userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(self.chip['FIFO'].get_data()))
                        v_adc = LSB*(userk_data['Data'][0])
                        voltages_internal.append(v_adc) 
                        #time.sleep(2)                     
		        print('*******************')
                        print 'Vmonitor value: ', vmonitor
		        print(name)
			print 'Voltage measured with internal ADC : ', v_adc
		        print '*******************'
                        #raw_input("enter")
                        if len(userk_data) > 0:
                            break
                else:
                    self.logger.error('Timeout while waiting for chip status.')

            #CURRENTS IN TABLE 29
            for cmonitor in np.	arange(0, 26, 1):
                self.chip.get_ADC(typ='I', address=cmonitor)
                name = self.chip.current_mux[cmonitor]
                for _ in range(timeout):
                    if self.chip['FIFO'].get_FIFO_SIZE() > 0:
                        userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(self.chip['FIFO'].get_data()))
                        i_adc = LSB*(userk_data['Data'][0])
                        currents_internal.append(i_adc)
                        #time.sleep(2) 
		        print'*******************'
                        print 'Cmonitor value: ', cmonitor
		        print(name)
			print 'Voltage measured with internal ADC : ', i_adc
		        print '*******************'
                        #raw_input("enter")
                        if len(userk_data) > 0:
                            break
                else:
                    self.logger.error('Timeout while waiting for chip status.')
        except:
            self.logger.error('There was an error while receiving the chip status.')
        self.chip._reset_ADC()
        #print voltages_internal, currents_internal, voltages_external, currents_external

	#Writting ADC voltage values:
	out_adc_v = open( directory + '/'+name_file_adc_v, "a")	
	tobewritten = timestamp + array2string(voltages_internal) +  "\n"
	out_adc_v.write(tobewritten) 
	out_adc_v.close()

	#Writting ADC current values:
	out_adc_i = open( directory + '/'+name_file_adc_i, "a")	
	tobewritten = timestamp + array2string(currents_internal) + "\n"
	out_adc_i.write(tobewritten) 
	out_adc_i.close()



        return voltages_internal, currents_internal, 1, 1


    def main(self, **kwargs):
        #for x in range(1):
        voltages_internal, currents_internal, voltages_external, currents_external = self.get_adc_mux_multimeter()





if __name__ == '__main__':
    ADC_scan = ADC_scan('default_chip.yaml')
    #ADC_scan = ADC_scan_multimeter('default_chip.yaml')
    ADC_scan.main(**local_configuration)
