from bdaq53.rd53a import RD53A
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.fifo_readout import FifoReadout
from Ldr_Temp_readout import Temperature_readout
import matplotlib.dates as mdate
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.dates import YearLocator, MonthLocator,HourLocator, DayLocator , DateFormatter
import matplotlib.pyplot as plt
#from bdaq53.serial_com import serial_com
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import pytemperature
import time
import os
import math
import datetime
import sys
import calendar
import numpy as np
import logging
import yaml
import time
import re
import csv
from matplotlib import pyplot as plt
from Ldr_Temp_monitor import meas_temperature
from matplotlib import dates
from matplotlib.dates import AutoDateFormatter, AutoDateLocator
# variables/lists
raw_adc = []
averages = []
Sensors  = []
Filterout_numbers = [1,2,3,4]# for filtering out umwanted char
SenseArraysize = 16
d_config = 0


if __name__ == "__main__":

    # Calibration parameters
    k = 1.38064852*math.pow(10,-23) #
    q = 1.60217662*math.pow(10,-19) #
    V_refADC_SCC203 = 0.911 #[V]    #
    LSB = V_refADC_SCC203/math.pow(2, 12) #
    R = np.log(15)
    # NTC_absolut = pytemperature.c2k(NTC)
    constant = 4285.2

    # # Calculation Nf for each sensor
    # Nf_sensors = [(i * LSB)/(((k*NTC_absolut)/(q))*R) for i in sensors]
    # sensors.insert(0,NTC)
    # sensors.insert(0, time.time())
    directory = '00_TEST_INFO/temperature_sensor/processed_data'
    if not os.path.exists(directory):
        os.makedirs(directory)
    inputfile_DeltaV = (directory +'/' + "temperature_sensor_delta_V.txt")


    f=open(inputfile_DeltaV,"r")
    lines=f.readlines()
    Time = []
    NTC_T = []
    Tsens1, Tsens2, Tsens3, Tsens4, Rsens1, Rsens2, Rsens3, Rsens4 = ([] for i in range(8))
    for x in lines:
        print len(x.split(','))
        if len(x.split(',')) == 10:     
            Time.append(x.split(',')[0]); NTC_T.append(x.split(',')[1])
            Tsens1.append(x.split(',')[2]); Tsens2.append(x.split(',')[3])
            Tsens3.append(x.split(',')[4]); Tsens4.append(x.split(',')[5])
            Rsens1.append(x.split(',')[6]);Rsens2.append(x.split(',')[7])
            Rsens3.append(x.split(',')[8]); Rsens4.append(x.split(',')[9])
    f.close()
    Time = map(float, Time); NTC_T = map(float, NTC_T)
    Tsens1 = map(float, Tsens1); Tsens2 = map(float, Tsens2)
    Tsens3 = map(float, Tsens3); Tsens4 = map(float, Tsens4)
    Rsens1 = map(float, Rsens1); Rsens2 = map(float, Rsens2)
    Rsens3 = map(float, Rsens3); Rsens4 = map(float, Rsens4)
   # test = Nf_sensors[0]


    Tsens1 = np.array(pytemperature.k2c((constant/Nf_sensors[0])*LSB*np.array(Tsens1))); Tsens2 = pytemperature.k2c((constant/Nf_sensors[1])*LSB*np.array(Tsens2))
    Tsens3 = pytemperature.k2c((constant/Nf_sensors[2])*LSB*np.array(Tsens3)); Tsens4 = pytemperature.k2c((constant/Nf_sensors[3])*LSB*np.array(Tsens4))
    Rsens1 = pytemperature.k2c((constant/Nf_sensors[4])*LSB*np.array(Rsens1)); Rsens2 = pytemperature.k2c((constant/Nf_sensors[5])*LSB*np.array(Rsens2))
    Rsens3 = pytemperature.k2c((constant/Nf_sensors[6])*LSB*np.array(Rsens3)); Rsens4 = pytemperature.k2c((constant/Nf_sensors[7])*LSB*np.array(Rsens4))



    # convert time to datetime
    Datetime = []
    for val in Time:
        Datetime.append(mdate.epoch2num(val))

    temprad = [Tsens1, Tsens2, Tsens3, Tsens4,Rsens1,Rsens2,Rsens3,Rsens4]
    legendtemp = ['tempsens1','tempsens2','tempsens3','tempsens4','radsens1','radsens2','radsens3','radsens4']
    #ax1 = fig.add_subplot(111)


    locator = AutoDateLocator()
    formatter = AutoDateFormatter(locator)


    fig, (ax1) = plt.subplots( figsize=(15, 8))
    for i in range(len(temprad)):
        ax1.plot_date(Datetime,temprad[i],'o',label = legendtemp[i])#, label = legend[i],s=0.5, color = line_color[i])
        ax1.xaxis.set_major_locator(locator)
        ax1.xaxis.set_major_formatter(formatter)
    ax1.plot_date(Datetime,NTC_T,'k-',label = "NTC", linewidth = 4.0) #, label = legend[i],s=0.5, color = line_color[i])
    date_fmt = '%d-%m-%H-%M'
    ax1.set_ylim((-100,100))

    ax1.autoscale_view()
    fig.autofmt_xdate()
    ax1.legend(loc=4)
    ax1.set_title('All 8 temp/rad sensors + NTC | Calibration temp: auto')
    ax1.set_xlabel('Time [Auto]')
    ax1.set_ylabel('Temperature (C)')

    fig.subplots_adjust(hspace=0)
    #plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)
    plt.grid(b=None, which='minor', axis='both')
    plt.savefig(directory +'/' + "AllSensor_last_update.PNG", dpi=1000)
    plt.show(block=False)
    plt.pause(10)
    plt.close()




    #Temperature_sensors = [(constant/i)*(LSB*np.array(tempsen for i in sensors]
