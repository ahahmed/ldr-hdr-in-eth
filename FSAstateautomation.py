#-------------------------------------------------------------------------------
# LDR FSA-lib. class 
# Author: Ahmed Abdirashid 
#-------------------------------------------------------------------------------

class supervisor_scheduler:
    def __init__(self):
        self.handlers = {}
        self.startState = None
        
    def include_state(self, name, handler):
        name = name.upper()
        self.handlers[name] = handler

    def ILDE_entry(self, name):
        self.startState = name.upper()

    def begin_supervisors(self):
        try:
            handler = self.handlers[self.startState]

        except Exception as e:
            logging.error(".include_state() are missing", exc_info=False)

        while True:
            try:
                newState = handler()
                handler = self.handlers[newState.upper()] 
            except Exception as e:
                pass
